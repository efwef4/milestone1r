package Boi3;

import robocode.*;

import java.awt.*;







public class Boi3 extends AdvancedRobot

{


    
    double orbit = 150;

  
    double goToX;

    double goToY;



    public void run() {

      
   
      
        goToX = getBattleFieldWidth()/2;

        goToY = getBattleFieldHeight()/2;




 
    }



    public void onScannedRobot(ScannedRobotEvent e) {

  
        goToX = e.getDistance()*Math.cos(Math.PI/2-(e.getBearingRadians()+getHeadingRadians()))+getX();

        goToY = e.getDistance()*Math.sin(Math.PI/2-(e.getBearingRadians()+getHeadingRadians()))+getY();

       
 goToOrbit(goToX, goToY);

      
     

     
          
            

  
          


        scan();

    }
	
  public void onHitWall(HitWallEvent e) {
     
     back(10);
    }

 
    public void onHitRobot(HitRobotEvent e) {
   
      fire(10);
    }


    public void donWin(WinEvent e) {

    ahead(0);
    turnGunRight(0);
    for ( int i = 0; i < 50; i++) {
        turnRight(30);
        turnLeft(30);
    }
}

 
    public double normalRelativeAngleDegrees( double angle) {
        while (angle<-180)
            angle+=360;
        while (angle>=180)
            angle-=360;
        return (angle);
    }



    
     public void onBulletHit(BulletHitEvent e) {
      back(10);
      fire(10);
        }
    


    public void goToOrbit( double orbitX, double orbitY) {
   
        double turnRightArg = normalRelativeAngleDegrees(Math.atan2(orbitY-getY(), getX()-orbitX)*180/Math.PI-getHeading()-90);
        double orbitDistance = Math.sqrt(Math.pow(orbitX-getX(), 2)+Math.pow(orbitY-getY(), 2));

     

     
        setTurnRight(normalRelativeAngleDegrees(turnRightArg));


    
        }
   
}




